#include <avr/io.h>
#include <util/delay.h>
#include "segments.h"
#include <avr/interrupt.h>
#include <avr/iotn13.h>
#define delay 1

volatile uint8_t min5=0;
volatile uint8_t h=1;
volatile uint16_t hz5=0;
//volatile uint8_t hz5=0;
void hour();
void min();
void taster();



int main()
{
	
	sei();
	TCCR0A=0;
	TCNT0=12;
	BIT_SET(TIMSK0,TOIE0);
	//start timer prescaler 1024
	BIT_SET(TCCR0B,CS02);
	BIT_SET(TCCR0B,CS00);
	while(1)
	{
		taster();
		hour();
		min();
	}
	
	


}

void taster()
{
	DDRB=0;   
	PORTB=31;
	if(!BIT_CHECK(PINB,PINB0))
	{
		while(!BIT_CHECK(PINB,PINB0))
		{	/*
			if(!BIT_CHECK(PINB,PINB4))  //+h
			{
			while(!BIT_CHECK(PINB,PINB0)){}
				_delay_ms(5);
				h++;
			}
			*/
		}
		_delay_ms(5);
		min5++;
		hz5=0;
		if(min5>=12)
		{
			min5=0;
		}
		else if(min5==4)
		{
			h++;
		}
	}
}

ISR(TIM0_OVF_vect)
{
	TCNT0=12;
	if(hz5<=1438)
	{
		hz5++;
	}
	else
	{
		hz5=0;
		min5++;
		if(min5>=12)
		{
			min5=0;
		}
		else if(min5==4)
		{
			h++;
		}
	}
}

void min()
{
	if(min5==0||min5==12)
	{
		return;
	}
	else if(min5==11) //55min
	{
		fuenf();
		_delay_ms(delay);
		VOR();
		_delay_ms(delay);
	}
	else if(min5==10) //50min
	{
		zehn();
		_delay_ms(delay);
		VOR();
		_delay_ms(delay);
	}
	else if(min5==9){ //45min
		fuenf();
		_delay_ms(delay);
		zehn();
		_delay_ms(delay);
		VOR();
		_delay_ms(delay);
	}
	else if(min5==8){ //40min
		zehn();
		_delay_ms(delay);
		NACH();
		_delay_ms(delay);
		HALB();
		_delay_ms(delay);
	}
	else if(min5==7){ //35min
		fuenf();
		_delay_ms(delay);
		NACH();
		_delay_ms(delay);
		HALB();
		_delay_ms(delay);
	}
	else if(min5==6){ //30min
		HALB();
		_delay_ms(delay);

	}
	else if(min5==5) //25min
	{
		fuenf();
		_delay_ms(delay);
		VOR();
		_delay_ms(delay);
		HALB();
		_delay_ms(delay);
	}
	else if(min5==4) //20min
	{
		zehn();
		_delay_ms(delay);
		VOR();
		_delay_ms(delay);
		HALB();
		_delay_ms(delay);
	}
	else if(min5==3)//15min
	{
		fuenf();
		_delay_ms(delay);
		zehn();
		_delay_ms(delay);
		NACH();
		_delay_ms(delay);
	}
	else if(min5==2)//10min
	{
		zehn();
		_delay_ms(delay);
		NACH();
		_delay_ms(delay);
	}
	else if(min5==1)
	{
		fuenf();
		_delay_ms(delay);
		NACH();
		_delay_ms(delay);
	}

}

void hour()
{
	if(h==1)
	{
		EINS();
	}
	else if(h==2)
	{
		ZWEI();
	}
	else if(h==3)
	{
		DREI();
	}
	else if(h==4)
	{
		VIER();
	}
	else if(h==5)
	{
		FUENF();
	}
	else if(h==6)
	{
		SECHS();
	}
	else if(h==7)
	{
		SIEBEN();
	}
	else if(h==8)
	{
		ACHT();
	}
	else if(h==9)
	{
		NEUN();
	}
	else if(h==10)
	{
		ZEHN();
	}
	else if(h==11)
	{
		ELF();
	}
	else if(h==12)
	{
		ZWOELF();
	}
	else
	{
		h=0;
	}
	_delay_ms(delay);
}
