/*
 * segments.h
 *
 *  Created on: 08.09.2014
 *      Author: Jan
 */

#ifndef SEGMENTS_H_
#define SEGMENTS_H_


#define fuenf() PORTB=16;DDRB=17
#define zehn()  PORTB=16;DDRB=18
#define VOR()   PORTB=16;DDRB=20
#define NACH()  PORTB=16;DDRB=24

#define eins()  PORTB=8;DDRB=9
#define zwei()  PORTB=8;DDRB=10
#define vier()  PORTB=8;DDRB=12

#define HALB()  PORTB=8;DDRB=24

#define EINS()  PORTB=4;DDRB=5
#define ZWEI()  PORTB=4;DDRB=6
#define DREI()	PORTB=4;DDRB=12
#define VIER()  PORTB=4;DDRB=20
#define FUENF() PORTB=2;DDRB=3
#define SECHS() PORTB=2;DDRB=6
#define SIEBEN() PORTB=2;DDRB=10
#define ACHT()  PORTB=2;DDRB=18
#define NEUN()  PORTB=1;DDRB=3
#define ZEHN()  PORTB=1;DDRB=5
#define ELF()   PORTB=1;DDRB=9
#define ZWOELF() PORTB=1;DDRB=17

#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))

#endif /* SEGMENTS_H_ */
